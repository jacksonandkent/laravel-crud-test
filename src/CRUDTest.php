<?php
namespace JacksonAndKent;

trait CRUDTest
{

    public function test_crud_workflow()
    {
        if (method_exists($this, "setup_CRUD_data")) {
            $this->setup_CRUD_data();
        }

        $this->login()
            ->navigateToListPageFromLogin()
            ->addANewItem()
            ->seeItemInList()
            ->navigateToEditPageAfterCreation()
            ->seePreviousDataInFields()
            ->changePreviousData()
            ->seeChangedItemInList()
            ->navigateToEditPageAfterUpdate()
            ->editItemAndSeeChanges()
            ->gotoNewAndSeeFieldsErrorOnSubmit()
            ->gotoEditAndSeeFieldsErrorOnSubmit()
            ->otherVerificationBeforeDelete() // Par exemple si le nouvel utilisateur peut se connecter
            ->navigateToEditPageAfterUpdate()
            ->deleteItem()
            ->dontSeeItemInList();
    }

    public function todo($msg){
        $this->fail("/!\ **** CRUDTest - TODO **** /!\ \n".$msg);
    }

    public function login()
    {
        if (method_exists($this, "loginAsAdmin")) {
            $this->loginAsAdmin();

            return $this;
        } else {
            $this->todo('$this->loginAsAdmin() n\'existe pas, vous devez implémenter la méthode login() dans votre classe de test.');
        }

    }

    public function navigateToListPageFromLogin()
    {
        if (!isset($this->listPageLinkName)) {
            $this->todo('Vous devez indiquer le nom du lien pour la page qui liste les éléments.');
        }

        if (!isset($this->listPageRouteName)) {
            $this->todo('Vous devez indiquer le nom de la route pour la page qui liste les éléments.');
        }

        if (!isset($this->listPageRouteParam)) {
            $this->listPageRouteParam = [];
        }

        return $this->click($this->listPageLinkName)
            ->seePageIs(route($this->listPageRouteName, $this->listPageRouteParam));
    }

    public function addANewItem()
    {
        $this->todo("Vous devez définir la méthode 'addANewItem()'.\nElle doit partir de la page liste, cliquer sur le lien d'ajout, remplir le formulaire, cliquer sur le bouton d'envoi et vérifier qu'on revient à la page liste.");
    }

    public function seeItemInList()
    {
        if (!isset($this->fieldsAndValueToSeeOnListPageAfterCreation)) {
            $this->todo('Vous devez indiquer les champs visibles sur la page qui liste les éléments après la création, avec leur valeur.');
        }

        foreach ($this->fieldsAndValueToSeeOnListPageAfterCreation as $fieldName => $filedValue) {
            $this->seeInElement("." . $fieldName, $filedValue);
        }

        return $this;
    }

    public function navigateToEditPageAfterCreation()
    {
        return $this->navigateToEditPage("creation");
    }

    public function navigateToEditPageAfterUpdate()
    {
        return $this->navigateToEditPage("update");
    }

    public function navigateToEditPage($context)
    {
        if (!isset($this->fieldsAndValueToSeeOnListPageAfterCreation)) {
            $this->todo('Vous devez indiquer les champs visibles sur la page qui liste les éléments après la création, avec leur valeur.');
        }

        if (!isset($this->editPageRouteName)) {
            $this->todo('Vous devez indiquer le nom de la route pour la page qui permet d\'éditer un élément.');
        }

        if (!isset($this->editPageRouteParam)) {
            $this->todo('Vous devez indiquer les paramètres nécessaire accéder à la page d\'édition d\'un élément (l\'id par exemple).');
        }

        if (!isset($this->linkLibToClickOnListPage)) {
            $this->todo('Vous devez indiquer l\'attribut dont la valeur doit être cliqué sur la page de liste pour accéder à la page d\'édition.');
        }

        // On récupère le premier élément cherché sur la page
        if($context == "creation")
            $linkLib = $this->fieldsAndValueToSeeOnListPageAfterCreation[$this->linkLibToClickOnListPage];
        else
            $linkLib = $this->fieldsAndValueToSeeOnListPageAfterUpdate[$this->linkLibToClickOnListPage];

        return $this
            ->visit(route($this->listPageRouteName, $this->listPageRouteParam))
            ->click($linkLib)
            ->seePageIs(route($this->editPageRouteName, $this->editPageRouteParam));
    }

    public function seePreviousDataInFields()
    {
        $this->todo("Vous devez définir la méthode 'seePreviousDataInFields()'.\nElle doit vérifier que les champs du formulaire d'édition de l'élément contiennent les données de l'élément.");
    }

    public function changePreviousData()
    {
        $this->todo("Vous devez définir la méthode 'changePreviousData()'.\nElle doit remplir le formulaire avec des données différentes de la création et vérifier qu'on revient sur la page de liste.");
    }

    public function seeChangedItemInList()
    {
        if (!isset($this->fieldsAndValueToSeeOnListPageAfterUpdate)) {
            $this->todo('Vous devez indiquer les champs visibles sur la page qui liste les éléments après la mise à jour, avec leur valeur.');
        }

        foreach ($this->fieldsAndValueToSeeOnListPageAfterUpdate as $fieldName => $filedValue) {
            $this->seeInElement("." . $fieldName, $filedValue);
        }

        return $this;
    }

    public function editItemAndSeeChanges()
    {
        $this->todo("Vous devez définir la méthode 'editItemAndSeeChanges()'.\nElle doit vérifier que les champs contiennent les bonnes nouvelles valeures.");
    }

    public function gotoNewAndSeeFieldsErrorOnSubmit()
    {
        $this->todo("Vous devez définir la méthode 'gotoNewAndSeeFieldsErrorOnSubmit()'.\nElle doit partir de la page liste, cliquer sur le lien d'ajout, NE PAS remplir le formulaire, cliquer sur le bouton d'envoi et vérifier qu'on affiche les erreurs de champ obligatoire.\nElle doit aussi vérifier que les champs dont le format doit être respecté affichent une erreur.");
    }

    public function otherVerificationBeforeDelete()
    {
        $this->todo("Vous devez définir la méthode 'otherVerificationBeforeDelete()'.\nElle permet de faire d'autres vérification avant la suppression de l'élément.\nPar exemple, si le CRUD porte sur des utilisateur, elle permet de vérifier que le nouvel utilisateur peut se connecter.");
    }

    public function deleteItem()
    {
        return $this        
            ->click("deleteBtn")
            ->seePageIs(route($this->listPageRouteName, $this->listPageRouteParam));
    }

    public function dontSeeItemInList()
    {

        if (!isset($this->fieldsAndValueNotToSeeOnListPageAfterDelete)) {
            $this->todo('Vous devez indiquer les champs visibles sur la page qui liste les éléments après la suppression, avec les valeurs qui ne doivent pas apparaître.');
        }

        foreach ($this->fieldsAndValueNotToSeeOnListPageAfterDelete as $fieldName => $filedValue) {
            $this->dontSeeInElement("." . $fieldName, $filedValue);
        }

        return $this;
        
    }
}
