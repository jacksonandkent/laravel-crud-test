# Laravel-CRUD-Test
Ce repo contient un trait d'aide à la création de test pour faire un CRUD.

Il est fait pour travailler en TDD, il va permettre de vérifier les étapes suivante :

- Identification
- Navigation jusqu'à la page de liste des éléments
- Ajout d'un élément
- Vérification d'affichage du nouvel élément
- Edition de l'élément
- Vérification de la bonne remontée des valeurs
- Changement des valeurs et enregistrement
- Vérification d'affichage des nouvelles valeures
- Edition de l'élément
- Vérification de la bonne remontée des nouvelles valeurs
- Vérification de l'affichage des erreurs sur le formulaire d'ajout
- Vérification de l'affichage des erreurs sur le formulaire de mise à jour
- Autre vérifications à faire avant la suppression de l'élément (par exemple, si le nouvel utilisateur peut se connecter)
- Vérification de la possibilité de supprimer l'élément
- Vérification que l'élément n'est plus listé

## Installation

### Ajouter le package au composer.json
Editer /composer.json pour ajouter dans require-dev :

    "jacksonandkent/laravel-crud-test": "dev-master"

Et ajouter un noeud en frère de require :

    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/jacksonandkent/laravel-crud-test.git"
        }
    ]

Dire à composer d'installer ce package :

    composer update laravel-crud-test

### Comment l'utiliser ?
Créer une classe de test qui utilise ce trait :

	<?php

	use JacksonAndKent\CRUDTest;

	class WhateverNameTest {

		use CRUDTest;

		public function login(){
			// Ne pas oublier de retourner $this pour la prochaine fonction
			return $this->actingAs($user);
		}

	}

Lancer les tests, ils vont indiquer le nom de la méthode manquante, qu'il faut définir et faire fonctionner avant d'arriver jusqu'à la méthode d'après et ainsi de suite.

Certaines méthodes sont déjà incluses dans le trait pour limiter le travail. Elle demande alors à renseigner des attributs à votre classe pour les utiliser.

Ces attributs sont à définir dans une méthode appelée "setup_CRUD_data" :

    public function setup_CRUD_data()
    {
        $this->listPageLinkName                           = "linkToAdminUsersList";
        $this->listPageRouteName                          = "adminUsersList";
        $this->listPageRouteParam                         = [];
        $this->linkLibToClickOnListPage                   = "login";
        $this->fieldsAndValueToSeeOnListPageAfterCreation = [
            'login'      => 'Gaius',
            'role'       => 'Administrateur',
            'created_at' => Carbon::now()->format("d/m/Y"),
        ];
        $this->editPageRouteName                        = "adminUsersEdit";
        $this->editPageRouteParam                       = 1;
        $this->fieldsAndValueToSeeOnListPageAfterUpdate = [
            'login'      => 'William',
            'role'       => 'Modérateur',
            'created_at' => Carbon::now()->format("d/m/Y"),
        ];
    }

Ne pas hésiter à surcharger ces méthodes si elle ne conviennent pas à votre situation.